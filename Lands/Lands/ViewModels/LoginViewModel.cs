﻿namespace Lands.ViewModels
{

    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using System.ComponentModel;

    public class LoginViewModel : BaseViewModel
    {

        #region Atributes

        private string _password = string.Empty;
        private bool _isRunning = false;
        private bool _isEnabled = false;

        #endregion

        #region Properties

        public string Email
        {
            get;
            set;
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                SetValue(ref _password, value);
            }
        }

        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                SetValue(ref _isRunning, value);
            }
        }

        public bool IsRemember
        {
            get;
            set;
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                SetValue(ref _isEnabled, value);
            }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        private async void Login()
        {
            //Se verifica si el campo Email está vacio o es Nulo.
            if (string.IsNullOrEmpty (this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error", 
                    "You must enter an Email", 
                    "Accept");
                return;
            }

            //Se verifica si el campo Password está vacio o es Nulo.
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter a Password",
                    "Accept");
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            if (this.Email != "luisenrique.vl@gmail.com" || this.Password != "123456")
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Email or Password incorrect",
                    "Accept");
                this.Password = string.Empty;
                this.IsRunning = false;
                this.IsEnabled = true;
                return;
            }

            await Application.Current.MainPage.DisplayAlert(
                    "OK",
                    "Correct",
                    "Accept");
            return;
        }

        #endregion

        #region Constructors

        public LoginViewModel()
        {
            this.IsRemember = true;
            this._isEnabled = true;
        }

        #endregion

    }
}
